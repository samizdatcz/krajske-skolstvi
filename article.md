---
title: "Předvolební analýza: Kraje zachraňují učební obory. Žáci o ně nemají zájem"
perex: "V krajských volbách se hraje také o podobu středních škol. Největší část krajských rozpočtů směřuje právě do školství. Jaké nástroje mají budoucí hejtmani k prosazování svých slibů?"
description: "V krajských volbách se hraje také o podobu středních škol. Největší část krajských rozpočtů směřuje právě do školství. Jaké nástroje mají budoucí hejtmani k prosazování své představy o školství?"
authors: ["Jan Boček"]
published: "3. října 2016"
coverimg: https://interaktivni.rozhlas.cz/krajske-skolstvi/media/cover.jpg
coverimg_note: "Foto Bill Green: <a href='https://www.flickr.com/photos/leicamini/25147365172/'>Welder</a> (CC BY-NC 2.0)"
url: "krajske-skolstvi"
libraries: [jquery, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/cestina/
    title: Data o znalostech češtiny
    perex: Čechům dělá největší problém psaní velkých písmen, nejlépe zvládají vyjmenovaná slova.
    image: https://interaktivni.rozhlas.cz/cestina/media/decko.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/rusove-proti-rusum-zeme-v-nevyhlasene-obcanske-valce--1484099
    title: Rusové proti Rusům: Země v nevyhlášené občanské válce
    perex: Rusko prošlo v devadesátých letech demografickou krizí srovnatelnou s válečným konfliktem.
    image: http://media.rozhlas.cz/_obrazek/3372356.jpeg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/jizni-mesto-sidliste-bez-lidi--1532519
    title: Jižní Město – sídliště bez lidí?
    perex: Skoro deset tisíc lidí zmizelo z největšího tuzemského sídliště od roku 2001. Kam a proč?
    image: https://samizdat.blob.core.windows.net/storage/jizni-mesto-1.jpg
---

V roce 2012 studovalo v Karlovarském kraji na šesti nebo osmiletých gymnáziích 21 procent ze všech středoškoláků. Dnes je to 24 procent – navzdory intenzivním snahám ministerstva školství i krajských politiků vysvětlovat, že na víceletá gymnázia patří jen elita.

Tři procentní body vypadají jako detail. Jenže v době, kdy na střední školy přicházejí početně nejslabší ročníky v historii, můžou znamenat rozdíl mezi přežitím a koncem některých oborů. Dobře to ilustrují počty tříd, které v Karlovarském kraji od roku 2012 musely skončit: na gymnáziích necelá dvě procenta, na odborných školách s maturitou zmizelo téměř dvacet procent tříd.

<aside class="big">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/data/krajske-skolstvi/www/media/zajem.png" width="100%">
  </figure>
  <figcaption>
    Zdroj: <a href="http://toiler.uiv.cz/rocenka/rocenka.asp" target="_blank">MŠMT</a>
  </figcaption>
</aside>

## Zájem o gymnázia se krajům nelíbí

Stejná nebo podobná situace se opakuje ve všech krajích. Ty jsou ze zákona zřizovatelem celé sítě středních škol, takže se snaží počty studentů na gymnáziích, maturitních a učňovských oborech vybalancovat. Posilování gymnázií znamená méně odborných studentů a učňů, proto současný vývoj většina krajů vnímá jako velmi špatnou zprávu.

„Pokles zájmu o učební obory souvisí s požadavkem státu na vysoké počty absolventů gymnázií a rovněž vysokých škol v minulých letech,“ shrnuje současný postoj většiny krajů jihomoravská mluvčí Eliška Holešínská Windová. „Výsledkem je snížení úrovně vysokého i středního školství. Odstranění negativních dopadů, včetně změny obecného povědomí, bude dlouhodobé,“ tvrdí Windová.

Nejvážněji prázdné třídy hrozí na Vysočině. Právě tady je nedostatek 15-19letých, tedy typických středoškoláků, nejsilnější. Podle prognózy Českého statistického úřadu z roku 2013 tu navíc krize potrvá ze všech krajů nejdéle: do roku 2019 má počet dětí a čerstvě dospělých klesnout až na 85 procent současného stavu, jejich počty přitom klesají už několik let. Statistici tvrdí, že přes dnešní počet 15-19letých se Vysočina – minimálně do roku 2050 – nedostane.

Na opačném pólu populační statistiky je Středočeský kraj. Tam má 15-19letých začít přibývat už od příštího roku, v roce 2027 jich má mít dokonce o 40 procent více než dnes.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/krajske-skolstvi/www/charts/prognoza.html" class="ig" width="100%" height="800px" scrolling="no" frameborder="0"></iframe>
</aside>

Pro přesnost je nutné dodat, že na víceletá gymnázia nastupují také 11 nebo 13letí studenti, na nástavbové a zkrácené studium zase osmnáctiletí a starší studenti. Graf tyto věkové skupiny neukazuje: jednak jich je oproti hlavní skupině 15-19letých poměrně málo, jednak by u nich křivky vypadaly velmi podobně.

## Dlouhodobá strategie se co pět let otočí

Krajští politici a úředníci ovšem nemusí nečinně sedět a čekat, jak se s demografickou krizí vyrovnají jeho školy. Mají k dispozici řadu nástrojů, kterými mohou síť škol ovlivňovat. V první řadě takzvaný dlouhodobý záměr vzdělávání – strategický dokument, který definuje směr rozvoje škol.

V něm ovšem kraje nejsou zcela svobodné: musejí se řídit centrálním dlouhodobým záměrem, který jednou za čtyři až pět let vydává ministerstvo školství. Kraje kopírují jeho zrovna platné teze a jen částečně je mohou doplnit o specifika kraje. V Moravskoslezském tak například přidávají poznámku o tom, že přes sedmdesát procent obyvatel zaměstnává průmysl, a proto je zde žádoucí technické vzdělání, Plzeň zase mluví o silném exportu a zdůrazňuje nutnost jazykového vzdělávání.

Centrální vzdělávací záměr je svým způsobem záznamem doby: [v roce 2007](http://www.msmt.cz/ministerstvo/dlouhodoby-zamer-vzdelavani-a-rozvoje-vzdelavaci-soustavy-cr), kdy se na ministerstvu školství střídali zelení ministři, řešil zejména vzdělávání zdravotně postižených, prevenci rasismu, práci s mimořádně nadanými studenty, rozvoj kompetencí namísto encyklopedických znalostí nebo svobodu škol učit po svém skrz vlastní výukové plány. [Plán na roky 2011 až 2015](http://www.msmt.cz/file/18719/download/), kterým se kraje řídily za dosluhujících hejtmanů, vznikal za ministrování Josefa Dobeše z Věcí veřejných a oproti předchozímu dokumentu zcela otočil: jeho hlavním tématem byly právě vysoké počty studentů na maturitních oborech a nutnost přizpůsobit se požadavkům zaměstnavatelů, zejména v průmyslu. Navrhoval mimo jiné snížit podíl středoškoláků na gymnáziích a v oborech s maturitou pod 68 procent (v roce 2011, kdy plán vznikal, jich v Česku bylo přes 70 procent).

[Aktuální záměr](http://www.msmt.cz/vzdelavani/skolstvi-v-cr/dlouhodoby-zamer-vzdelavani-a-rozvoje-vzdelavaci-soustavy-3), kterým se kraje řídí od letoška, vznikl pod sociální demokratkou Valachovou a na předchozí dokument opět navazuje jen vágně. Na rozdíl od svého předchůdce neurčuje, kolik studentů má mít maturitu a kolik ne; místo toho řeší například standardizaci přijímacích zkoušek na střední školy, celostátní maturity a rovnost příležitostí ve vzdělávání.

## Kraj drží nad vodou budoucí zedníky nebo svářeče

Ve vleku ministerstva školství je i další „tvrdý“ nástroj, financování škol. Ani tady nemají kraje velkou autonomii, přímo rozhodují přibližně o dvaceti procentech peněz, které do krajských škol proudí. I u těch se navíc řídí spíš akutními potřebami škol než svou představou, jak by měla síť škol vypadat. Na prosazování strategií, určených dlouhodobým záměrem, putuje méně než jedno procento z celkového rozpočtu.

Centrální databáze, která by dostatečně podrobně a srozumitelně evidovala krajské rozpočty, neexistuje. Proto se podrobněji podíváme pouze na jeden rozpočet: veškeré veřejné finance, které putovaly do škol zřizovaných Moravskoslezským krajem ve školním roce 2014/2015.

<aside class="big">
  <iframe src="https://interaktivni.rozhlas.cz/data/krajske-skolstvi/www/charts/rozpocet.html" class="ig" width="100%" height="800px" scrolling="no" frameborder="0"></iframe>
</aside>

Víc než polovinu pětimiliardového rozpočtu spolkly náklady na platy a povinné odvody. Ty do krajských škol putují z ministerstva školství. Kraj pak na základě [vyhlášky](http://www.zakonyprolidi.cz/cs/2005-492) spočítá, kolik peněz míří na jednotlivé obory a školy. Při výpočtu je podstatným ukazatelem naplněnost tříd; právě jeho pomocí – při výpočtu lze změnit naplněnost o plus minus deset procent – může kraj mírně zasáhnout do rozpočtů svých škol. Kromě toho si má možnost lehce přes tři procenta z ministerských peněz odložit jako rezervu a z ní pak například podpořit obory vzdělání, které chce zachovat nebo podpořit. Dohromady tak kraj dokáže ovlivnit z ministerských dotací pro školy jen několik procent.

Celková částka, kterou kraj pro své školy dostane, se ale nezmění: pokud kraj někomu přidá, musí jinde ubrat. Přerozdělování navíc kraje obvykle používají spíš v nouzi – například pro záchranu některého oboru – než jako běžný nástroj dlouhodobé školské politiky.

Přímo z rozpočtu kraje putovalo do škol zřizovaných krajem něco přes 800 milionů, tedy asi 15 procent. Z toho naprostá většina – přes 750 milionů – směřovala na provoz škol, nutné opravy a investice. Kraje se opět soustředí hlavně na to, aby jim školy bez rozdílu oboru nespadly pod rukama. Určitý manévrovací prostor už ale v této položce mají, zejména při plánování investic. Mohou například dát přednost školám, které mají podle dlouhodobého záměru hrát v krajském školství větší roli.

Na přímou podporu dlouhodobého záměru putovalo v Moravskoslezském kraji 45 milionů, méně než jedno procento celkového rozpočtu. Z těchto peněz kraj financuje například takzvané málopočetné obory – třídy, které by bez mimořádné finanční pomoci zanikly. Jde obvykle o učňovské obory, například zedník nebo svářeč. Kraje se snaží nepopulární obory zachránit také aktivní propagací, část peněz proto směřuje na stipendia, kampaně, ceny pro talentované studenty nebo pořádání oborových střetnutí, jako je – když zůstaneme na Ostravsku – mezinárodní soutěž zručnosti Zlatý pilník, soutěž Mladý malíř nebo Dřevorubec Junior. Jejich efekt je ovšem omezený, jak přiznávají i ostatní kraje.

„Kraj obecně nemá možnost jak poptávku po vybraných oborech ovlivnit,“ stěžuje si Eliška Holešínská Windová, mluvčí Jihomoravského kraje. „Kraj sice může ze svého rozpočtu poskytovat stipendia, v zájmu o příslušnou školu a obor ale nesehrávají zásadní roli.“ Podobně se vyjádřili i zástupci ostatních oslovených krajů – Karlovarského, Plzeňského a Ústeckého.

## Jednotné přijímačky mají naplnit učební obory, zatím se to nedaří

Důležitou „tvrdou“ pravomocí hejtmana je možnost zřizovat a rušit školy. Jde ale spíš o výjimečnou záležitost: v posledních čtyřech letech hejtmani v celém Česku zrušili 43 z původních 1 347 škol, tedy něco přes tři procenta (při čtrnáctiprocentním úbytku 15-19letých). Obvyklejší odpovědí na klesající stavy studentů je rušení tříd a to je v kompetenci ředitelů škol. Jediné větší jednorázové rušení škol proběhlo v posledních čtyřech letech na Vysočině, kde po školním roce 2013/14 zmizelo 10 ze 75 škol.

Karlovarský kraj v minulých letech otestoval další nástroj, který mu má pomoci ovlivnit poptávku: zavedením obtížnějších přijímacích zkoušek na gymnázia se pokusil přesměrovat méně nadané studenty na obory s maturitou a učební obory. Podobně to ostatně zkusilo i dalších sedm krajů. Setrvale rostoucí počet studentů karlovarských gymnázií zatím efekt těžších přijímaček neprokázal, ale na hodnocení může být ještě brzo. Jednotné přijímací zkoušky nicméně inspirovaly ministerstvo školství, to brzy plánuje spustit centrální přijímací testy pro všechny kraje.

Poslední nástroj, kterým mohou kraje prosazovat školskou politiku, je jmenování a odvolávání ředitelů škol nebo školské rady. Ti ovšem řeší spíš konkrétní problémy vlastní školy, než aby prosazovali nápady krajských politiků. Navíc procházejí běžným výběrovým řízením a mají šestileté funkční období, takže fungují poměrně samostatně.

Volba hejtmana tedy znamená, co se týče krajských škol, spíš volbu správce než politika, který by prosazoval vlastní představu. Na zavádění vlastní školské politiky nemá kraj mnoho nástrojů, většinou pouze prosazuje vůli ministerstva školství. Výjimkou může být rušení nebo zřizování škol, ale právě za nadcházejících hejtmanů by se měly stavy středoškoláků stabilizovat a tyto nástroje by neměly mít větší využití.



